﻿using System;
using System.Collections.Generic;

namespace _SmartCheckerWeb
{
    public class ExactMatch : IPlainPattern
    {
        public string Name { get { return "Точное совпадение"; } }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            string basicAnswer = basicAnswers[0];
            string answer = studentAnswers[0];
            return string.Equals(basicAnswer.Trim(), answer.Trim(), StringComparison.CurrentCultureIgnoreCase);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
