﻿using System.Collections.Generic;
using System.Text;

namespace _SmartCheckerWeb
{
    public class BasicGen
    {
        public List<IPlainPattern> PatternList { get; set; }
        public void ParseBasicAnswer(string basicAnswer, ref char delimiter, ref int startVert)
        {
            foreach (var c in basicAnswer)
                if (!char.IsDigit(c))
                {
                    delimiter = c;
                    break;
                }
            string[] vertices = basicAnswer.Split(delimiter);
            foreach (var vert in vertices)
            {
                int v = int.Parse(vert);
                if (v == 0)
                {
                    startVert = 0;
                    break;
                }
            }
        }
        public void AddVert(int num, Queue<int> q, bool[] used)
        {
            q.Enqueue(num);
            used[num] = true;
        }
        public List<List<int>> ParseTest(string test, out int rows)
        {
            List<List<int>> graph = new List<List<int>>();
            test = test.Replace("\r", "");
            string[] lines = test.Split('\n');
            rows = int.Parse(lines[0]);

            for (int i = 1; i <= rows; i++)
            {
                string line = lines[i];
                var curList = new List<int>();
                string[] values = line.Split(' ');
                foreach (var v in values)
                {
                    if (string.IsNullOrEmpty(v))
                        continue;
                    curList.Add(int.Parse(v));
                }
                graph.Add(curList);
            }
            return graph;
        }
        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            bool result = false;
            for (int i = 0; !result && i < basicAnswers.Count; i++)
            {
                string basicAnswer = basicAnswers[i];
                result = new ExactMatch().IsAnswerCorrect(studentAnswers, new List<string>() { basicAnswer });
            }
            return result;
        }
    }
    public class DepthFirstSearch : BasicGen, IGenPattern
    {
        public string Name { get { return "Обход в глубину"; } }
        List<List<int>> graph;
        bool[] used;
        char delimiter = ' ';
        int startVert = 1;

        public List<string> GenBasicAnswers(string test, string basicAnswer)
        {

            ParseBasicAnswer(basicAnswer, ref delimiter, ref startVert);

            int rows;
            graph = ParseTest(test, out rows);

            List<string> basicAnswers = new List<string>();
            for (int j = 0; j < rows; j++)
            {
                StringBuilder answer = new StringBuilder();
                used = new bool[rows];
                DFS(j, answer);
                answer.Remove(answer.Length - 1, 1);
                basicAnswers.Add(answer.ToString());
            }
            return basicAnswers;
        }

        void DFS(int vertex, StringBuilder answer)
        {
            used[vertex] = true;

            answer.Append(vertex + startVert);
            answer.Append(delimiter);
            if (PatternList == null)
                PatternList = new List<IPlainPattern>();
            PatternList.Add(new ExactMatch());

            for (int i = 0; i < graph.Count; i++)
            {
                if (graph[vertex][i] == 0)
                    continue;
                int unvisited = i;
                if (!used[unvisited])
                    DFS(unvisited, answer);
            }

        }
        public override string ToString()
        {
            return Name;
        }
    }

    public class BreadthFirstSearch : BasicGen, IGenPattern
    {
        public string Name { get { return "Обход в ширину"; } }
        public List<string> GenBasicAnswers(string test, string basicAnswer)
        {
            char delimiter = ' ';
            int startVert = 1;
            ParseBasicAnswer(basicAnswer, ref delimiter, ref startVert);

            int rows;
            List<List<int>> graph = ParseTest(test, out rows);

            List<string> basicAnswers = new List<string>();
            for (int j = 0; j < rows; j++)
            {
                StringBuilder answer = new StringBuilder();
                Queue<int> q = new Queue<int>();
                bool[] used = new bool[rows];

                AddVert(j, q, used);
                while (q.Count != 0)
                {
                    int cur = q.Dequeue();
                    if (PatternList == null)
                        PatternList = new List<IPlainPattern>();
                    PatternList.Add(new ExactMatch());
                    answer.Append(cur + startVert);
                    answer.Append(delimiter);

                    for (int i = 0; i < rows; i++)
                    {
                        if (graph[cur][i] == 0)
                            continue;
                        int neighbor = i;
                        if (!used[neighbor])
                            AddVert(neighbor, q, used);
                    }
                }
                answer.Remove(answer.Length - 1, 1);
                basicAnswers.Add(answer.ToString());
            }
            return basicAnswers;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
